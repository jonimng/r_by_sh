v1 <- c(1,2,3,4,5)
v2 <- c(5,6,7,8,9)

mat1 <- rbind(v1,v2)
mat2 <- cbind(v1,v2)
mat1 <- rbind(v1,v2)

mat2[v1]
mat1 [1,]


mat1[,3]

mat1[2,4]

#transpose 

t(mat2)


# Amtrix is still a vector 
mat1[3]


class(mat1)
typeof(mat1)


#random numbers

#Normal distrubtion 

dnorm(0,0,0.5)
qnorm(0.54,mean = 0, sd = 1)
pnorm(2,0,1)
rnorm(10,0,1)

dunif(3,max=10,min=2)
qunif(0.54,max=10,min=0.5)
punif(0.54)
mean(rnorm(1000000,0,1))

runif(10)

e <- runif(10)*10

#Graphics
data <- matrix(c(1:6,c(10,15,19,26,32,37)),6,2)

plot(data[,1], data[,2], type = "l", ylim = c(0,40))


#Histogram 

u <- runif(1000,0,100)
hist(u)


n <- rnorm(1000,0,100)
hist(n)

par(mfrow = c(2,1))
par(mfrow = c(2,2),mar=c(1,1,1,1))
plot(c(1,2,3),c(1,2,4), type = "l")
plot(c(1,2,3),c(1,2,4), type = "l")

#functions 

toFar <- function(cel){
  x <- cel*1.8
  x + 32
}

toFar(0)

toFar1 <- function(cel){
  x <- cel*1.8 + 32
  return(x)
}

toFar1(0)


derivf <- function(x){
  (f(x + 0.01) - f(x))/0.01
}

f <- function(x){
  x^3 + x^2 + x + 10
}

derivf(5)

derivf(c(1,3,6,11))

seq(0,20, length = 100) 

y <- f(x)
y.derive <- derivf(x)

par(mfrow = c(2,1))
plot(x,y, type = "l")
plot(x,y.derive, type = "l")


#Lists 

l <- list(owner = 'Jack','alic', sum = 3000)


l[['owner']]
l[[1]]
l$owner


# $ notation 


l$owner
l$sum

#data frame 

brands <- c('Ford', 'Mazda', 'Fiat')
from <- c('US', 'Japan', 'Italy')
rank <- c(3,1,2)


cars <- data.frame(brands, from, rank)

class(cars)
typeof(cars)


cars$brands
cars$from
cars$rank

typeof(cars$rank)

barnds.filter <- cars$brands == 'Ford'

cars[barnds.filter, c(1,3)]

summary(cars)
str(cars)

#reading data frame from a file 

worms <- read.table("worms.txt", header = T)

worms$Damp
