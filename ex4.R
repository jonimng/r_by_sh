titanic <- read.csv("train.csv")
str(titanic)

#Q2 - Are there attributes with missing values? If there are fill the missing values with the mean value of that attribute 
any(is.na(titanic)) #True

ageNotNa <- subset(titanic$Age, !is.na(titanic$Age))
any(is.na(ageNotNa))
titanic$Age[which(is.na(titanic$Age))] <- mean(ageNotNa)
any(is.na(titanic)) #False

#Q3 - Turn Survival into a factor with the right values  
titanic$Survived <- as.factor(titanic$Survived)
titanic$Survived <- factor(titanic$Survived, levels = c(0,1), labels = c("No", "Yes"))

#Q4 - Use ggplot histogram to investigate how Age, Fare and SibSp affect Survival
library(ggplot2)
#Age & Survivel
histogramAS <- ggplot(titanic, aes(x=titanic$Age, fill=titanic$Survived)) + geom_histogram(alpha=0.2, position="identity", binwidth = 0.5)

#Fare & Survivel
histogramFS <- ggplot(titanic, aes(x=titanic$Fare, fill=titanic$Survived)) + geom_histogram(alpha=0.2, position="identity", binwidth = 0.5) + xlim(0, 300)

#Silibings & Survivel
histogramSS <- ggplot(titanic, aes(x=titanic$SibSp, fill=titanic$Survived)) + geom_histogram(alpha=0.2, position="identity", binwidth = 0.5)
