# a number is evaluated to a vector 
5


# x is a variable "<-" is an assignment operator
x <- 5
x[1] #vector starts at 1 


#c - the 'combine' operator creates vectors 
x <- c(1,5,7.5)

#vectors are composed of items of the SAME data type
class(x)

#type casting 
y <- as.integer(x)

class(y)

# casting to character 
z <-as.character(y)


#subsetting and sequence operator
a <- 1:3

a[1:2]

a[c(1:2,1:2)]


#names for a vector items 
salary <- c(8000, 6000, 4000, 4000)
names(salary) <- c('Jack','John', 'Jerry', 'Alice')
salary['Jack']


#logical operators on vectors


x <-c(4,6,100,600)

x>7

#using logical operatons to filter vectors 
x[x>7]


#reversing a vector 
rev(x)

# sort from smallest to largest (defualt)
z <- c(8,9,4,5) 

sort(z)

order(z)
z[order(z)]


#sequence function 
seq(1,4,length=7)

#smaple function 
sample(x,10,replace = T)


# missung values (NA) 
z <-c(x,NA)

mean(z)

mean(z, na.rm = T)

mean(z[!is.na(z)])

#matrices

#matrix function creates a mtrix from a vector 
y <- matrix(c(1,2,3,4,5,6), nrow= 2,ncol=3 , byrow=T)


#subsetting a matrix 
y[,c(2,3)]


#negative indexes
y[-1,]
